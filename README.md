# Online-Bookstore

This is a simple online bookstore application built using Spring Boot and Hibernate.

## Features
- Manage books, authors, and genres.
- Perform CRUD operations on books.
- Search for books by title, author, or genre.

## Prerequisites
Before you begin, ensure you have met the following requirements:
- Java 17
- Maven
- Database MySQL
- Spring Boot

## Getting Started
To run the application, follow these steps:

1. Clone the repository
2. Navigate to the project directory:cd online-bookstore
3. Configure the database connection in the `application.properties` file.
4. Build and run the application using Maven: mvn spring-boot:run


The application should start, and you can access the API endpoints.

## Configuration
You can configure the application by editing the `application.properties` file in the `src/main/resources` directory. Here, you can set the database connection, server port, and other properties.

## API Endpoints
- **Books:**
  - GET `/api/books/` - Get a list of all books.
  - GET `/api/books/{id}` - Get a book by ID.
  - POST `/api/books/` - Create a new book.
  - PUT `/api/books/{id}` - Update a book by ID.
  - DELETE `/api/books/{id}` - Delete a book by ID.

- **Authors:**
  - GET `/api/authors/` - Get a list of all authors.
  - GET `/api/authors/{id}` - Get an author by ID.
  - POST `/api/authors/` - Create a new author.
  - PUT `/api/authors/{id}` - Update an author by ID.
  - DELETE `/api/authors/{id}` - Delete an author by ID.

- **Genres:**
  - GET `/api/genres/` - Get a list of all genres.
  - GET `/api/genres/{id}` - Get a genre by ID.
  - POST `/api/genres/` - Create a new genre.
  - PUT `/api/genres/{id}` - Update a genre by ID.
  - DELETE `/api/genres/{id}` - Delete a genre by ID.

## Testing
You can run tests using Maven:mvn test

## Feedback
-Was it easy to complete the task using AI? 
 Yes, it is more easy
- How long did task take you to complete? (Please be honest, we need it to gather anonymized statistics) 
it took about 3hrs.
- Was the code ready to run after generation? What did you have to change to make it usable?
it was not ready imediately but after a few changes it was ready
- Which challenges did you face during completion of the task?
the most challenge was the DB Configuration and Testing.

- Which specific prompts you learned as a good practice to complete the task?
I explain the context to the AI for better results




## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
